/**
 * Call a function and display result in a DOM Element.
 * @param func The function to call.
 * @param eltID The DOM Element's ID.
 */
var callFuncAndDisplayRes = function (func, eltID) {
    try {
        document.getElementById(eltID).innerHTML = func();
        document.getElementById(eltID).style.color ='black';
    } catch (e) {
        document.getElementById(eltID).innerHTML = e.message;
        document.getElementById(eltID).style.color ='red';
    }
};

// ### TASK 1 ###
/**
 * Computes the two possible ages to a given birth's year.
 * @param {Number} birthYear The birth's year.
 * @return {String} Output result.
 */
var calculateAge = function (birthYear) {
    var outputMessage = "";
    var age = new Date().getFullYear() - birthYear;
    if (isNaN(birthYear)) throw Error("Birth year needs to be a Number.");
    if (age < 0) throw Error("You can't be born in " + birthYear);
    else if (age === 0) outputMessage = "You aren't one year old yet. ";
    else outputMessage = "You are either " + (age - 1) + " or " + age + " years old.";
    console.log(outputMessage);
    return outputMessage;
};

try {calculateAge(1995)} catch (e) {console.log(e)}
try {calculateAge(2017)} catch (e) {console.log(e)}
try {calculateAge(2018)} catch (e) {console.log(e)}


// ### TASK 2 ###
/**
 * Computes a life time supply based on a current age and an amount per day.
 * @param age The current age.
 * @param amountPerDay The amount per day.
 * @return {String} Output result.
 */
var calculateSupply = function (age, amountPerDay) {
    var outputMessage = "";
    var MAX_AGE = 80;
    if (isNaN(age) || isNaN(amountPerDay) ) throw Error("The Age and the amount per day needs to be a Number.");
    if (age < 0) throw Error("You can't have a negative age.");
    if (age >= MAX_AGE) throw Error("You can't be older than the max age.");
    if (amountPerDay < 0) throw Error("You can't have a negative amount per day");
    var today = new Date();
    var lastDayOfSupply = new Date();
    lastDayOfSupply.setFullYear(today.getFullYear() + MAX_AGE - age);
    var nbOfDays = ((lastDayOfSupply - today) / (1000 * 60 * 60 * 24));
    var supply = Math.round(nbOfDays * amountPerDay);
    outputMessage = "You will need " + supply + " to last you until the ripe old age of " + MAX_AGE;
    console.log(outputMessage);
    return outputMessage;
};

try {calculateSupply(21, 4.78)} catch (e) {console.log(e)}
try {calculateSupply(21, -10)} catch (e) {console.log(e)}
try {calculateSupply(-15, 3)} catch (e) {console.log(e)}

// ### TASK 3 ###
/**
 * Computes the circumference of a circle with the radius.
 * @param radius The radius.
 * @return {String} Output result.
 */
var calcCircumference = function (radius) {
    var outputMessage = "";
    if (isNaN(radius)) throw Error("Radius needs to be a Number.");
    if (radius < 0) throw Error("You can't have a negative radius.");
    var circumference = 2 * Math.PI * radius;
    outputMessage = "The circumference is " + circumference + " unit.";
    console.log(outputMessage);
    return outputMessage;
};

/**
 * Computes the area of a circle with the radius.
 * @param radius The radius.
 * @return {String} Output result.
 */
var calcArea = function (radius) {
    var outputMessage = "";
    if (isNaN(radius)) throw Error("Radius needs to be a Number.");
    if (radius < 0) throw Error("You can't have a negative radius.");
    var area = Math.PI * radius * radius;
    outputMessage = "The area is " + area + " unit².";
    console.log(outputMessage);
    return outputMessage;
};

try {calcCircumference(2.35)} catch (e) {console.log(e)}
try {calcArea(2.35)} catch (e) {console.log(e)}
try {calcCircumference(-2)} catch (e) {console.log(e)}
try {calcArea(-2)} catch (e) {console.log(e)}

// ### TASK 4 ###
/**
 * Converts celsius to fahrenheit.
 * @param celsiusTemp Celsius temperature.
 * @return {String} Output result.
 */
var celsiusToFahrenheit = function (celsiusTemp) {
    var outputMessage = "";
    if (isNaN(celsiusTemp)) throw Error("Celsius temperature needs to be a Number.");
    var fahrenheitTemp = celsiusTemp * 9 / 5 + 32;
    outputMessage = celsiusTemp + "°C is " + fahrenheitTemp + "°F";
    console.log(outputMessage);
    return outputMessage;
};

/**
 * Converts fahrenheit to celsius.
 * @param fahrenheitTemp Fahrenheit temperature.
 * @return {String} Output result.
 */
var fahrenheitToCelsius = function (fahrenheitTemp) {
    var outputMessage = "";
    if (isNaN(fahrenheitTemp)) throw Error("Fahrenheit temperature needs to be a Number.");
    var celsiusTemp = (fahrenheitTemp - 32) * 5 / 9;
    outputMessage = fahrenheitTemp + "°F is " + celsiusTemp + "°C";
    console.log(outputMessage);
    return outputMessage;
};

try {celsiusToFahrenheit(15)} catch (e) {console.log(e)}
try {celsiusToFahrenheit(0)} catch (e) {console.log(e)}
try {celsiusToFahrenheit(-9)} catch (e) {console.log(e)}
try {fahrenheitToCelsius(59)} catch (e) {console.log(e)}
try {fahrenheitToCelsius(32)} catch (e) {console.log(e)}
try {fahrenheitToCelsius(15.8)} catch (e) {console.log(e)}

//Page smooth scrolling feature
$(document).ready(function () {
    $(".nav-item").click(function() {
        var page = $(this).find("a:first").attr("href");
        var speed = 750;
        $('html, body').animate({scrollTop: $(page).offset().top}, speed);
        return false;
    });
});
